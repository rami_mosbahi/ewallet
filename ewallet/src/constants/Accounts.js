export default {
    account1: {
        id: 1,
        name: 'Khabib',
        img: 'https://scontent.ftun4-1.fna.fbcdn.net/v/t1.15752-9/185529145_498513414725971_1078586566195856282_n.jpg?_nc_cat=106&ccb=1-3&_nc_sid=ae9488&_nc_ohc=eP3LFn8Zz7MAX-0JBu6&_nc_ht=scontent.ftun4-1.fna&oh=20d121359329979da2b09f213eca8dc9&oe=60C49A8D',
        country: 'Russia'
    },
    account2: {
        id: 2,
        name: 'Conor',
        img: 'https://static01.nyt.com/images/2020/01/17/sports/17ufc-01/merlin_167201508_ad1a782d-cfa7-4ed4-94ad-569938139d66-superJumbo.jpg',
        country: 'Ireland'
    },
    account3: {
        id: 3,
        name: 'Daniel',
        img: 'https://www.thirstyfornews.com/wp-content/uploads/2021/05/dana-white-44.jpg',
        country: 'USA'
    },

};