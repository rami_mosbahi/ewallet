import { View, ScrollView, ActivityIndicator } from 'react-native';
import React from 'react';
import styles from './styles'
import TitlePreviousScreen from '../../components/TitlePreviousScreen'
import FilterBarChart from '../../components/FilterBarChart'
import IncomeExpenseFilter from '../../components/IncomeExpenseFilter'
import BarChart from '../../components/BarChart'
import NewMessages from '../../components/NewMessages'
import TitleScreen from '../../components/TitleScreen'
import TransactionItem from '../../components/TransactionItem'



const StatisticScreen = ({ route, navigation }) => {
    const onPress = () => {
        navigation.goBack();
    }
    return (
        <ScrollView
            style={{
                backgroundColor: '#fafafa'
            }}
        >
            <View style={styles.container}>
                <TitlePreviousScreen
                    title={"Statistic"}
                    press={() => {
                        navigation.goBack();
                    }}
                />
                <FilterBarChart />
                <IncomeExpenseFilter />
                <BarChart />
                <NewMessages />
                <TitleScreen title={"Transactions"} />
                <TransactionItem
                    img={"https://ca-times.brightspotcdn.com/dims4/default/cb4eee7/2147483647/strip/true/crop/261x240+0+0/resize/840x772!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F46%2F97%2F9d0ab2fb461267d30905c403f309%2Fsdut-john-gordon-baden-20160823"}
                    name={"Leonardo"}
                    type={"Paying debt"}
                    price={"$240.00"}
                />
                <TransactionItem
                    img={"https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5cd1ab9ad606c400081eac42%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D0%26cropX2%3D416%26cropY1%3D0%26cropY2%3D416"}
                    name={"Facebook"}
                    type={"Endorsement"}
                    price={"$267.00"}
                />
                <TransactionItem
                    img={"https://pbs.twimg.com/profile_images/1255348148467314689/MZpN-g1j.jpg"}
                    name={"Affandi"}
                    type={"Project payment"}
                    price={"$267.00"}
                />

            </View>
        </ScrollView>
    );
};

export default StatisticScreen;
