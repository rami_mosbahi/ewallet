import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: '100%',
        backgroundColor: '#fafafa',
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: '5%',
        marginTop: 5
    },

})

export default styles;