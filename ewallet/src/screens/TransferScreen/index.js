import { Text, View, ScrollView } from 'react-native';

import React from 'react';
import Form from './Form'
const TransferScreen = ({ route, navigation }) => {
    const { account } = route.params;
    console.log(account)
    const onPress = () => {
        navigation.goBack();
    }
    return (
        <ScrollView
            style={{ backgroundColor: 'white' }}
        >
            <View
                style={{
                    backgroundColor: 'white',
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    paddingHorizontal: 5,
                    paddingVertical: 20

                }}
            >
                <Text
                    style={{
                        alignSelf: 'center',
                        fontSize: 20,
                        fontWeight: "bold",
                        marginBottom: 25,

                    }}
                >Transfer Form</Text>
                <Form navigation={navigation}
                    account={account}
                />
            </View>
        </ScrollView>
    );
};

export default TransferScreen;
