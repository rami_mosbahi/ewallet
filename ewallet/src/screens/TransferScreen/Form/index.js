import React, { useState, useEffect } from 'react';
import { Text, Modal, TouchableOpacity, View } from 'react-native';
import Auxilary from '../../../hoc/Auxilary'
import Input from '../../../components/common/Input'
import ModalPicker from '../../../components/common/Picker/ModalPicker'
import Button from '../../../components/common/Button'
import AppAlert from '../../../components/common/AppAlert'
import { expenseActions } from '../../../store/reducers/expense-slice'
import { useSelector, useDispatch } from 'react-redux'
import { addExpense } from '../../../store/actions/expense-action'
import { getAllCreditCards } from '../../../store/actions/creditCard-action'

import Account from '../../../constants/Accounts'



const Form = ({ navigation, account }) => {

    const dispatch = useDispatch();

    const [modalTypeIsVisible, setmodalTypeIsVisible] = useState(false)

    const destinationSelectedPicker = (option) => {
        dispatch(expenseActions.setDestinationSelected({ destinationSelected: option }));
    }
    const amountToTransferSelectedInput = (option) => {
        dispatch(expenseActions.setAmountToTransferSelected({ amountToTransferSelected: option }));
    }
    const typeSelectedInput = (option) => {
        dispatch(expenseActions.setTypeSelected({ typeSelected: option }));
    }
    const setDestinationCard = (option) => {
        dispatch(expenseActions.setDestinationCard({ destinationCardSelected: option }));
    }




    const currentCredit = useSelector((state) => state.creditCard.currentCreditCard);


    const destinationSelected = useSelector((state) => state.expense.destinationSelected);
    const amountToTransferSelected = useSelector((state) => state.expense.amountToTransferSelected);
    const typeSelected = useSelector((state) => state.expense.typeSelected);
    const destinationCardSelected = useSelector((state) => state.expense.destinationCardSelected);

    const errorTransferFormValidation = useSelector((state) => state.expense.errorTransferFormValidation);
    const showValidTransferFormValidation = useSelector((state) => state.expense.showValidTransferFormValidation);
    const errorTransferFormValidationMsg = useSelector((state) => state.expense.errorTransferFormValidationMsg);
    const creditCards = useSelector((state) => state.creditCard.creditCards);
    const creditCardDestination = useSelector((state) => state.creditCard.creditCardDestination);
    const allCreditCards = useSelector((state) => state.creditCard.allCreditCards);

    const listDestination = ['Khabib', 'Conor', 'Daniel'].filter(el => el != Account['account' + account.id].name)



    useEffect(() => {
        dispatch(getAllCreditCards())
    }, [])


    const submitTransfer = () => {

        var destination = allCreditCards.filter(el => el.id == destinationCardSelected)
        var destinationItem = destination[0] || {}




        var regType = /^[a-z]+$/i;
        if (destinationSelected == "Destination") {
            dispatch(expenseActions.setErrorTransferFormValidation(
                {
                    errorTransferFormValidation: true,
                    errorTransferFormValidationMsg: "Destination field is required !"
                }));
        }
        else
            if (destinationItem.id == currentCredit.id) {
                dispatch(expenseActions.setErrorTransferFormValidation(
                    {
                        errorTransferFormValidation: true,
                        errorTransferFormValidationMsg: "This is your card !!"
                    }));
            }
            else
                if (destinationCardSelected.length == 0) {
                    dispatch(expenseActions.setErrorTransferFormValidation(
                        {
                            errorTransferFormValidation: true,
                            errorTransferFormValidationMsg: "Destination card number is required"
                        }));
                }
                else
                    if (destinationCardSelected.length < 12) {
                        dispatch(expenseActions.setErrorTransferFormValidation(
                            {
                                errorTransferFormValidation: true,
                                errorTransferFormValidationMsg: "Destination card number is invalid"
                            }));
                    }
                    else
                        if (amountToTransferSelected.length == 0) {
                            dispatch(expenseActions.setErrorTransferFormValidation(
                                {
                                    errorTransferFormValidation: true,
                                    errorTransferFormValidationMsg: "Amount field is required !"
                                }));
                        }
                        else
                            if (typeSelected.length == 0) {
                                dispatch(expenseActions.setErrorTransferFormValidation(
                                    {
                                        errorTransferFormValidation: true,
                                        errorTransferFormValidationMsg: "Type is required !"
                                    }));
                            }
                            else
                                if (!regType.test(typeSelected)) {
                                    dispatch(expenseActions.setErrorTransferFormValidation(
                                        {
                                            errorTransferFormValidation: true,
                                            errorTransferFormValidationMsg: "Type is invalid !"
                                        }));
                                }
                                else
                                    if (parseInt(amountToTransferSelected) > parseInt(currentCredit.balance)) {
                                        dispatch(expenseActions.setErrorTransferFormValidation(
                                            {
                                                errorTransferFormValidation: true,
                                                errorTransferFormValidationMsg: "Amount is grander than your balance !"
                                            }));
                                    }

                                    else {




                                        dispatch(addExpense({
                                            destination: destinationSelected,
                                            destinationCard: destinationCardSelected,
                                            amount: amountToTransferSelected,
                                            type: typeSelected,
                                            from: currentCredit.id,

                                        }, currentCredit, destination[0]));
                                        dispatch(expenseActions.setShowValidTransferFormValidation(
                                            {
                                                showValidTransferFormValidation: true,
                                            }));
                                    }



    }




    return (
        <Auxilary>
            <AppAlert
                visible={errorTransferFormValidation}
                type={'failed'}
                msg={errorTransferFormValidationMsg}
                close={() => {
                    dispatch(expenseActions.setErrorTransferFormValidation(
                        {
                            errorTransferFormValidation: false,
                            errorTransferFormValidationMsg: ""
                        }));
                }}
            />
            <AppAlert
                visible={showValidTransferFormValidation}
                type={'success'}
                msg={"Success, your card has been aded !"}
                close={() => {
                    dispatch(expenseActions.resetSelectedForm());
                    dispatch(expenseActions.setShowValidTransferFormValidation(
                        {
                            showValidTransferFormValidation: false,
                        }));
                    navigation.goBack();
                }}
            />
            {/* Modals of pickers */}
            <Modal
                style={{
                    elevation: 20,
                }}
                animationType="fade"
                transparent={true}
                visible={modalTypeIsVisible}
                nRequestClose={
                    () => setmodalTypeIsVisible(false)
                }
            >
                <ModalPicker
                    changeModalVisible={setmodalTypeIsVisible}
                    setData={destinationSelectedPicker}
                    options={listDestination}
                />
            </Modal>

            {/* Type picker */}
            <TouchableOpacity

                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "space-between",
                    backgroundColor: 'white',
                    borderWidth: 1,
                    borderColor: '#adb5bd',
                    width: '100%',
                    height: 50,
                    borderRadius: 5,
                    paddingHorizontal: 10
                }}
                onPress={() => {
                    setmodalTypeIsVisible(true)
                }}
            >
                <Text>{destinationSelected}</Text>
                <Text> ▼</Text>
            </TouchableOpacity>
            {/* Card number input */}
            <Input
                error={false}
                onChangeText={(value) => {
                    setDestinationCard(value)
                }}
                value={destinationCardSelected}
                label="Last name"
                placeholder="Card number"
                type="numeric"
                maxLength={12}
                textAlign="center"
            />
            {/* Card number input */}
            <Input
                error={false}
                onChangeText={(value) => {
                    amountToTransferSelectedInput(value)
                }}
                value={amountToTransferSelected}
                label="Last name"
                placeholder="Amount to transfer"
                type="numeric"
                maxLength={12}
                textAlign="center"
            />
            {/* CVV of card */}
            <Input
                error={false}
                onChangeText={(value) => {
                    typeSelectedInput(value)
                }}
                value={typeSelected}
                label="Last name"
                placeholder="Category"

                type="default"
                textAlign="center"
            />
            {/* Submit button to add credit card */}
            <Button
                width={"100%"}
                color={"#1d8aef"}
                borderColor={"#1d8aef"}
                title={"Submit"}
                titleColor={"white"}
                press={() => {
                    submitTransfer()
                }}
            />
        </Auxilary>
    );
};

export default Form;