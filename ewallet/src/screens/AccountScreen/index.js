import { Text, View, TouchableOpacity, SafeAreaView, Image, ActivityIndicator } from 'react-native';

import React, { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';

import { ScrollView } from 'react-native-gesture-handler';

import TransactionItem from '../../components/TransactionItem'
import Accounts from '../../constants/Accounts'








const AllCardsScreen = () => {



    const navigation = useNavigation();
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.Header}>
                <Text
                    style={styles.TextTitle}
                >Select your account </Text>
                <TouchableOpacity
                    onPress={() => {

                    }}
                >
                    <Image
                        style={styles.addStyle}
                        source={{ uri: 'https://uxwing.com/wp-content/themes/uxwing/download/12-people-gesture/user-profile.png' }}
                    />
                </TouchableOpacity>
            </View>
            <ScrollView>
                <TransactionItem
                    img={Accounts.account1.img}
                    name={Accounts.account1.name}
                    type={Accounts.account1.country}
                    press={() => {
                        navigation.navigate("AllCards", { account: { id: Accounts.account1.id } });
                    }}
                />
                <TransactionItem
                    img={Accounts.account2.img}
                    name={Accounts.account2.name}
                    type={Accounts.account2.country}
                    press={() => {
                        navigation.navigate("AllCards", { account: { id: Accounts.account2.id } });
                    }}
                />
                <TransactionItem
                    img={Accounts.account3.img}
                    name={Accounts.account3.name}
                    type={Accounts.account3.country}
                    press={() => {
                        navigation.navigate("AllCards", { account: { id: Accounts.account3.id } });
                    }}
                />

            </ScrollView>

        </SafeAreaView>
    );
};

export default AllCardsScreen;
