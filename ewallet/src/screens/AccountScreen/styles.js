import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafafa',
        flex: 1,
        flexDirection: 'column',
        // padding: windowWidth / 11.4408


    },
    Header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
        paddingVertical: '5%',


    },
    AllCards: {
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },
    TextTitle: {
        fontSize: 24,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    addStyle: {
        width: windowWidth / 9.3800,
        height: windowWidth / 9.3800,
        borderRadius: 20,
        resizeMode: "cover",
    },
    DeleteLogo: {
        width: windowWidth / 9.3800,
        height: windowWidth / 9.3800,
        resizeMode: "cover",
    },


})

export default styles;