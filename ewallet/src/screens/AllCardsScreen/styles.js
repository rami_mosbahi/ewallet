import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafafa',
        flex: 1,
        flexDirection: 'column',
        // padding: windowWidth / 11.4408


    },
    Header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
        paddingVertical: '5%',


    },
    AllCards: {
        flexDirection: 'column',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },
    TextTitle: {
        fontSize: 24,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    addStyle: {
        width: windowWidth / 9.3800,
        height: windowWidth / 9.3800,
        borderRadius: 20,
        resizeMode: "cover",
    },
    DeleteLogo: {
        width: windowWidth / 9.3800,
        height: windowWidth / 9.3800,
        resizeMode: "cover",
    },
    styleDeleteLogo: {
        alignItems: 'center',
        backgroundColor: '#fafafa',
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 189,
        width: '90%',
        marginTop: 0,
        borderRadius: 15,
    },
    styleImgDeleteLogo: {
        height: '100%',
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    }


})

export default styles;