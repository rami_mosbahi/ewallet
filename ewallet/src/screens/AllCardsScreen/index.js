import { Text, View, TouchableOpacity, SafeAreaView, Image, ActivityIndicator } from 'react-native';

import React, { useState, useEffect } from 'react';
import styles from './styles';
import CreditCard from '../../components/CreditCard'
import AppAlert from '../../components/common/AppAlert'
import ConfirmAlert from '../../components/common/ConfirmAlert'

import { SwipeListView } from 'react-native-swipe-list-view';
import deleteLogo from '../../assets/img/delete.png'
import addLogo from '../../assets/img/add.png'
import { useSelector, useDispatch } from 'react-redux'
import { fetchCreditCardData, deleteCreditCard, setCurrentCreditCardAction } from '../../store/actions/creditCard-action'
import { creditCardActions } from '../../store/reducers/creditCard-slice'

const AllCardsScreen = ({ route, navigation }) => {
    const { account } = route.params;
    const dispatch = useDispatch();
    const [showConfirmAlert, setShowConfirmAlert] = useState(false)
    const [selectedItem, setSelectedItem] = useState(null)
    const creditCards = useSelector((state) => state.creditCard.creditCards);
    const errorCreditCards = useSelector((state) => state.creditCard.errorCreditCards);
    const errorCreditCardsMsg = useSelector((state) => state.creditCard.errorCreditCardsMsg);
    const errorToAddCard = useSelector((state) => state.creditCard.errorToAddCard);
    const loadingCreditCards = useSelector((state) => state.creditCard.loadingCreditCards);


    const deleteCardHandler = (id) => {
        setSelectedItem(id)
        setShowConfirmAlert(true)
    }

    const setErrorCreditCardsHandler = () => {
        dispatch(creditCardActions.setErrorCreditCards({
            errorCreditCards: false,
            errorCreditCardsMsg: ""
        }));
    }
    const addCard = () => {

        if (creditCards.length >= 3) {
            dispatch(creditCardActions.setErrorToAddCard({ errorToAddCard: true }))

        }
        else {
            navigation.navigate("FormCard", { account });
        }
    }



    useEffect(() => {
        dispatch(fetchCreditCardData(account.id));
    }, [])
    useEffect(() => {
        return () => {
            dispatch(creditCardActions.resetAllCards());
        }
    }, [])





    return (
        <SafeAreaView style={styles.container}>
            <AppAlert
                visible={errorToAddCard}
                type={'failed'}
                msg={"You can earn only 3 cards !"}
                close={() => { dispatch(creditCardActions.setErrorToAddCard({ errorToAddCard: false })) }}
            />
            <AppAlert
                visible={errorCreditCards}
                type={'failed'}
                msg={errorCreditCardsMsg}
                close={setErrorCreditCardsHandler}
            />
            <ConfirmAlert
                visible={showConfirmAlert}
                msg={"Are you sure about that"}
                close={() => {
                    setShowConfirmAlert(false)
                }}
                confirm={() => {

                    dispatch(deleteCreditCard(selectedItem))
                    setShowConfirmAlert(false)
                }}
                cancel={() => {
                    setShowConfirmAlert(false)
                }}
            />
            <View style={styles.Header}>
                <Text
                    style={styles.TextTitle}
                >My wallet </Text>
                <TouchableOpacity
                    onPress={() => {
                        addCard()
                    }}
                >
                    <Image
                        style={styles.addStyle}
                        source={addLogo}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.AllCards}
            >
                {
                    loadingCreditCards ?
                        <ActivityIndicator size="large" color="#0000ff" />
                        :
                        creditCards.length == 0 ?
                            <Text>No data</Text>
                            :
                            <SwipeListView
                                data={creditCards}
                                renderItem={

                                    (item, rowMap) => (
                                        <CreditCard
                                            navigationTo={() => {
                                                dispatch(setCurrentCreditCardAction(JSON.parse(JSON.stringify(item.item))))
                                                navigation.navigate("Dashboard", { data: JSON.parse(JSON.stringify(item)), account: account });
                                            }}
                                            data={item.item}
                                        />
                                    )
                                }
                                renderHiddenItem={(data, rowMap) => (
                                    <TouchableOpacity
                                        onPress={() => {
                                            deleteCardHandler(data.item.id)

                                        }}
                                        style={styles.styleDeleteLogo} >
                                        <View
                                            style={styles.styleImgDeleteLogo}
                                        >
                                            <Image
                                                style={styles.DeleteLogo}
                                                source={deleteLogo}
                                            />
                                        </View>


                                    </TouchableOpacity>
                                )}
                                leftOpenValue={170}
                                disableLeftSwipe
                            />
                }



            </View>
        </SafeAreaView>
    );
};

export default AllCardsScreen;
