import React, { useState } from 'react';
import { Text, Modal, TouchableOpacity, View } from 'react-native';
import Auxilary from '../../../hoc/Auxilary'
import Input from '../../../components/common/Input'
import ModalPicker from '../../../components/common/Picker/ModalPicker'
import Button from '../../../components/common/Button'
import AppAlert from '../../../components/common/AppAlert'
import { creditCardActions } from '../../../store/reducers/creditCard-slice'
import { useSelector, useDispatch } from 'react-redux'
import { addCreditCard } from '../../../store/actions/creditCard-action'




const Form = ({ navigation, account }) => {
    const dispatch = useDispatch();

    const [modalTypeIsVisible, setmodalTypeIsVisible] = useState(false)
    const [modalMMIsVisible, setmodalMMIsVisible] = useState(false)
    const [modalYYIsVisible, setmodalYYIsVisible] = useState(false)


    const changeModalVisible = (props) => {
        setModalIsVisible(props)
    }
    const setDataType = (option) => {
        dispatch(creditCardActions.setTypeCreditCardSelected({ typeCreditCardSelected: option }));
    }
    const setDataCardNumber = (option) => {
        dispatch(creditCardActions.setNumberCreditCardSelected({ numberCreditCardSelected: option }));
    }
    const setDataCardHolderName = (option) => {
        dispatch(creditCardActions.setHolderNameCreditCardSelected({ holderNameCreditCardSelected: option }));
    }
    const setDataMM = (option) => {
        dispatch(creditCardActions.setMmCreditCardSelected({ mmCreditCardSelected: option }));

    }
    const setDataYY = (option) => {
        dispatch(creditCardActions.setYyCreditCardSelected({ yyCreditCardSelected: option }));

    }
    const setDataCvv = (option) => {
        dispatch(creditCardActions.setCvvCreditCardSelected({ cvvCreditCardSelected: option }));
    }


    const typeCreditCardSelected = useSelector((state) => state.creditCard.typeCreditCardSelected);
    const numberCreditCardSelected = useSelector((state) => state.creditCard.numberCreditCardSelected);
    const holderNameCreditCardSelected = useSelector((state) => state.creditCard.holderNameCreditCardSelected);
    const mmCreditCardSelected = useSelector((state) => state.creditCard.mmCreditCardSelected);
    const yyCreditCardSelected = useSelector((state) => state.creditCard.yyCreditCardSelected);
    const cvvCreditCardSelected = useSelector((state) => state.creditCard.cvvCreditCardSelected);
    const creditCards = useSelector((state) => state.creditCard.creditCards);
    const errorFormValidation = useSelector((state) => state.creditCard.errorFormValidation);
    const errorFormValidationMsg = useSelector((state) => state.creditCard.errorFormValidationMsg);
    const showValidFormValidation = useSelector((state) => state.creditCard.showValidFormValidation);
    const currentCreditCard = useSelector((state) => state.creditCard.currentCreditCard);




    const expiredCard = (mm, yy) => {
        var currentYear = new Date().getFullYear().toString().substr(-2);
        var currentMonth = ("0" + (new Date().getMonth() + 1)).slice(-2)
        if (yy == currentYear) {
            if (mm < currentMonth) {
                return true
            }
            else {
                return false
            }
        }
        else
            if (yy < currentYear) {
                return true
            }
            else {
                return false
            }
    }


    const submitCreditCard = () => {
        var regHolderName = /^[a-z]+$/i;

        let filteredArray = creditCards.filter(el => el.number == numberCreditCardSelected);
        if (typeCreditCardSelected == "Type") {
            dispatch(creditCardActions.setErrorFormValidation(
                {
                    errorFormValidation: true,
                    errorFormValidationMsg: "Please select a card type !"
                }));

        }
        else
            if (filteredArray.length > 0) {
                dispatch(creditCardActions.setErrorFormValidation(
                    {
                        errorFormValidation: true,
                        errorFormValidationMsg: "This card is taken by another customer !"
                    }));

            }
            else
                if (numberCreditCardSelected.length == 0) {
                    dispatch(creditCardActions.setErrorFormValidation(
                        {
                            errorFormValidation: true,
                            errorFormValidationMsg: "Card number field is required !"
                        }));

                }
                else
                    if (numberCreditCardSelected.length < 12) {
                        dispatch(creditCardActions.setErrorFormValidation(
                            {
                                errorFormValidation: true,
                                errorFormValidationMsg: "This card number is invalid  !"
                            }));

                    }
                    else
                        if (holderNameCreditCardSelected.length == 0) {
                            dispatch(creditCardActions.setErrorFormValidation(
                                {
                                    errorFormValidation: true,
                                    errorFormValidationMsg: "holdername field is required"
                                }));

                        }
                        else
                            if (!regHolderName.test(holderNameCreditCardSelected)) {
                                dispatch(creditCardActions.setErrorFormValidation(
                                    {
                                        errorFormValidation: true,
                                        errorFormValidationMsg: "This holdername is invalid  !"
                                    }));

                            }
                            else
                                if (mmCreditCardSelected == "MM") {
                                    dispatch(creditCardActions.setErrorFormValidation(
                                        {
                                            errorFormValidation: true,
                                            errorFormValidationMsg: "Please select the expiration month !"
                                        }));

                                }
                                else
                                    if (yyCreditCardSelected == "YY") {
                                        dispatch(creditCardActions.setErrorFormValidation(
                                            {
                                                errorFormValidation: true,
                                                errorFormValidationMsg: "Please select the expiration year !"
                                            }));

                                    }
                                    else
                                        if (cvvCreditCardSelected.length == 0) {
                                            dispatch(creditCardActions.setErrorFormValidation(
                                                {
                                                    errorFormValidation: true,
                                                    errorFormValidationMsg: "CVV field is required"
                                                }));

                                        }
                                        else
                                            if (cvvCreditCardSelected.length < 3) {
                                                dispatch(creditCardActions.setErrorFormValidation(
                                                    {
                                                        errorFormValidation: true,
                                                        errorFormValidationMsg: "CVV is invalid"
                                                    }));

                                            }
                                            else {
                                                if (expiredCard(mmCreditCardSelected, yyCreditCardSelected)) {
                                                    dispatch(creditCardActions.setErrorFormValidation(
                                                        {
                                                            errorFormValidation: true,
                                                            errorFormValidationMsg: "This card is expired !"
                                                        }));
                                                }
                                                else {
                                                    dispatch(addCreditCard({
                                                        type: typeCreditCardSelected,
                                                        number: numberCreditCardSelected,
                                                        holderName: holderNameCreditCardSelected,
                                                        expirationMonth: mmCreditCardSelected,
                                                        expirationYear: yyCreditCardSelected,
                                                        cvv: cvvCreditCardSelected,
                                                        account: account
                                                    }));
                                                    dispatch(creditCardActions.setShowValidFormValidation(
                                                        {
                                                            showValidFormValidation: true,
                                                        }));
                                                }
                                            }
    }



    return (
        <Auxilary>
            <AppAlert
                visible={errorFormValidation}
                type={'failed'}
                msg={errorFormValidationMsg}
                close={() => {
                    dispatch(creditCardActions.setErrorFormValidation(
                        {
                            errorFormValidation: false,
                            errorFormValidationMsg: ""
                        }));
                }}
            />
            <AppAlert
                visible={showValidFormValidation}
                type={'success'}
                msg={"Success, your card has been aded !"}
                close={() => {
                    dispatch(creditCardActions.resetSelectedForm());
                    dispatch(creditCardActions.setShowValidFormValidation(
                        {
                            showValidFormValidation: false,
                        }));
                    navigation.goBack();
                }}
            />
            {/* Modals of pickers */}
            <Modal
                style={{
                    elevation: 20,
                }}
                animationType="fade"
                transparent={true}
                visible={modalTypeIsVisible}
                nRequestClose={
                    () => setmodalTypeIsVisible(false)
                }
            >
                <ModalPicker
                    changeModalVisible={setmodalTypeIsVisible}
                    setData={setDataType}
                    options={['MasterCard', 'Visa']}
                />
            </Modal>
            <Modal
                style={{
                    elevation: 20,
                }}
                animationType="fade"
                transparent={true}
                visible={modalMMIsVisible}
                nRequestClose={
                    () => setmodalMMIsVisible(false)
                }
            >
                <ModalPicker
                    changeModalVisible={setmodalMMIsVisible}
                    setData={setDataMM}
                    options={['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']}
                />
            </Modal>
            <Modal
                style={{
                    elevation: 20,
                }}
                animationType="fade"
                transparent={true}
                visible={modalYYIsVisible}
                nRequestClose={
                    () => setmodalYYIsVisible(false)
                }
            >
                <ModalPicker
                    changeModalVisible={setmodalYYIsVisible}
                    setData={setDataYY}
                    options={['21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32']}
                />
            </Modal>

            {/* Type picker */}
            <TouchableOpacity

                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: "space-between",
                    backgroundColor: 'white',
                    borderWidth: 1,
                    borderColor: '#adb5bd',
                    width: '100%',
                    height: 50,
                    borderRadius: 5,
                    paddingHorizontal: 10
                }}
                onPress={() => {
                    setmodalTypeIsVisible(true)
                }}
            >
                <Text>{typeCreditCardSelected}</Text>
                <Text> ▼</Text>
            </TouchableOpacity>

            {/* Card number input */}
            <Input
                error={false}
                onChangeText={(value) => {
                    setDataCardNumber(value)
                }}
                value={numberCreditCardSelected}
                label="Last name"
                placeholder="Card number "
                type="numeric"
                maxLength={12}
                textAlign="center"
            />
            {/* Card holder name input */}
            <Input
                error={false}
                onChangeText={(value) => {
                    setDataCardHolderName(value)
                }}
                value={holderNameCreditCardSelected}
                label="Last name"
                placeholder="Card holder name "
                textAlign="center"
            />
            {/* Expiration date of card */}
            <View
                style={{
                    flexDirection: 'row',
                }}
            >
                <TouchableOpacity

                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: "space-between",
                        backgroundColor: 'white',
                        borderWidth: 1,
                        borderColor: '#adb5bd',
                        width: '50%',
                        height: 50,
                        borderRadius: 5,
                        paddingHorizontal: 10
                    }}
                    onPress={() => {
                        setmodalMMIsVisible(true)
                    }}
                >
                    <Text>{mmCreditCardSelected} </Text>
                    <Text> ▼</Text>
                </TouchableOpacity>
                <TouchableOpacity

                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: "space-between",
                        backgroundColor: 'white',
                        borderWidth: 1,
                        borderColor: '#adb5bd',
                        width: '50%',
                        height: 50,
                        borderRadius: 5,
                        paddingHorizontal: 10,
                    }}
                    onPress={() => {
                        setmodalYYIsVisible(true)
                    }}
                >
                    <Text>{yyCreditCardSelected}</Text>
                    <Text>▼</Text>
                </TouchableOpacity>

            </View>
            {/* CVV of card */}
            <Input
                error={false}
                onChangeText={(value) => {
                    setDataCvv(value)
                }}
                value={cvvCreditCardSelected}
                label="Last name"
                placeholder="CVV"
                maxLength={3}
                type="numeric"
                textAlign="center"
            />
            {/* Submit button to add credit card */}
            <Button
                width={"100%"}
                color={"#1d8aef"}
                borderColor={"#1d8aef"}
                title={"Submit"}
                titleColor={"white"}
                press={() => {
                    submitCreditCard()
                }}
            />
        </Auxilary>
    );
};

export default Form;