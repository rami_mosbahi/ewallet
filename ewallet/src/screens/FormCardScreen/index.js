import { Text, View, ScrollView } from 'react-native';

import React from 'react';
import Form from './Form'
const FormCardScreen = ({ route, navigation }) => {
    const { account } = route.params;
    const onPress = () => {
        navigation.goBack();
    }
    return (
        <ScrollView
            style={{ backgroundColor: 'white' }}
        >
            <View
                style={{
                    backgroundColor: 'white',
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                    paddingHorizontal: 5,
                    paddingVertical: 20

                }}
            >
                <Text
                    style={{
                        alignSelf: 'center',
                        fontSize: 20,
                        fontWeight: "bold",
                        marginBottom: 25,

                    }}
                >Credit Card information</Text>
                <Form navigation={navigation}
                    account={account}
                />
            </View>
        </ScrollView>
    );
};

export default FormCardScreen;
