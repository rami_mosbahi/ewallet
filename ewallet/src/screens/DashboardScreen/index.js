import { Text, View, TouchableOpacity, SafeAreaView, Image, ScrollView } from 'react-native';

import React from 'react';
import styles from './styles';
import CreditCard from '../../components/CreditCard'
import RecentStats from '../../components/RecentStats'
import TitleScreen from '../../components/TitleScreen'
import HistoryItem from '../../components/HistoryItem'
import { getAllExpenses } from '../../store/actions/expense-action'
import { getAllIncomes } from '../../store/actions/income-action'

import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import Account from '../../constants/Accounts'

const DashboardScreen = ({ route, navigation }) => {
    const dispatch = useDispatch();

    const { account } = route.params;

    const toTransfer = () => {
        navigation.navigate("Transfer", { account: account });
    }


    const currentCredit = useSelector((state) => state.creditCard.currentCreditCard);
    const expenseDashboardisActive = useSelector((state) => state.expense.expenseDashboardisActive);


    useEffect(() => {

        if (expenseDashboardisActive) {
            dispatch(getAllExpenses())
        }
        else {
            dispatch(getAllIncomes())
        }

    }, [])


    return (
        <ScrollView
            style={{
                backgroundColor: '#fafafa'
            }}
        >
            <SafeAreaView style={styles.container}>

                <View style={styles.Header}>
                    <Text
                        style={styles.TextTitle}
                    >Dashboard</Text>
                    <TouchableOpacity>
                        <Image
                            style={styles.profilPicStyle}
                            source={{ uri: Account['account' + account.id].img }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.CardContainer}>
                    <CreditCard
                        data={currentCredit}
                        navigationTo={() => {
                        }}

                    />
                    <View
                        style={styles.containerButtons}
                    >

                        <TouchableOpacity
                            style={styles.styleButton}
                            onPress={toTransfer}
                        >
                            <Text>Transfer</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.styleButton}
                        >
                            <Text>Top up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <RecentStats
                    navigation={navigation}
                    data={currentCredit}
                />
                <TitleScreen title={"History"} />
                <HistoryItem />
            </SafeAreaView>
        </ScrollView>
    );
};

export default DashboardScreen;
