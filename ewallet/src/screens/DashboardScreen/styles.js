import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: '100%',
        backgroundColor: '#fafafa',
        flexDirection: 'column',
        alignItems: 'center',
        paddingHorizontal: '5%'



    },
    Header: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: 20,
        backgroundColor: '#fafafa'

    },
    CardContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#fafafa'

    },
    TextTitle: {
        fontSize: 24,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    profilPicStyle: {
        width: windowWidth / 8.3800,
        height: windowWidth / 8.3800,
        borderRadius: 10,
        resizeMode: "cover",
    },
    containerButtons: {
        width: 200,
        backgroundColor: 'white',
        marginTop: -22,
        height: 30,
        borderRadius: 11,
        shadowColor: '#b3b3b3',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 12,
        paddingVertical: 5,
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    styleButton: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 0.4,
    }


})

export default styles;