
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'
import Button from '../../components/common/Button'


const IncomeExpenseFilter = () => {
    return (
        <View style={styles.container}>
            <View style={styles.titlePart}>
                <Text style={styles.titleStyle}>
                    Income <Text style={styles.percentSubtitleTitleStyle}>↑15</Text>
                </Text>
                <Text style={styles.subTitleStyle}>$4354.00</Text>
            </View>
            <View style={styles.buttonPart}>
                <Button
                    width={"65%"}
                    color={"white"}
                    borderColor={"white"}
                    title={"Income ↑"}
                    titleColor={"#1d8aef"}
                    press={() => {
                    }}
                />
            </View>


        </View>
    );
};

export default IncomeExpenseFilter;
