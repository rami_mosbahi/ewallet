import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 10
    },
    titlePart: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: '#fafafa',
        height: '100%',
        width: '50%'


    },
    buttonPart: {
        backgroundColor: '#fafafa',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-end',
        height: '100%',
        width: '50%',
        paddingRight: 5

    },
    titleStyle: {
        fontSize: 17,
        color: 'grey',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    subTitleStyle: {
        fontSize: 25,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: 'bold'
    },
    percentSubtitleTitleStyle: {
        fontSize: 15,
        color: '#50cf4d',
        fontFamily: 'notoserif',
        fontWeight: '700'
    }

})

export default styles;