/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles'

const HistoryItem = ({ img, name, type }) => {
    return (
        <View style={styles.container}>
            <View
                style={styles.customerPart}
            >
                <Image
                    style={styles.customerImage}
                    source={{
                        uri: 'https://ca-times.brightspotcdn.com/dims4/default/cb4eee7/2147483647/strip/true/crop/261x240+0+0/resize/840x772!/quality/90/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F46%2F97%2F9d0ab2fb461267d30905c403f309%2Fsdut-john-gordon-baden-20160823',
                    }} />
                <View
                    style={styles.customerDetailsPart}
                >
                    <Text
                        style={styles.customerDetailsNamePart}
                    >Leonardo</Text>
                    <Text
                        style={styles.customerDetailsTypePart}
                    >Paying debit</Text>
                </View>
            </View>
            <View
                style={styles.amountPart}
            >
                <Text style={styles.amountTextStyle}>+$240.00</Text>

            </View>
        </View>
    );
};

export default HistoryItem;
