import React from 'react';
import { View, Text } from 'react-native';
import { VictoryBar, VictoryGroup, VictoryChart, VictoryAxis, VictoryLabel } from "victory-native";
import styles from './styles'



const BarChart = () => {
    return (
        <View style={styles.container}>
            <View style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "white"
            }}>

                <VictoryChart
                    width={350}
                    height={220}
                >
                    <VictoryAxis
                        style={{
                            tickLabels: { fill: 'grey' },
                            axis: { stroke: '#ffffff' }
                        }}

                    />
                    <VictoryAxis
                        style={{
                            tickLabels: { fill: '#1d8aef' },
                            axis: { stroke: '#ffffff' }
                        }}
                        tickValues={["Wed"]}
                    />
                    <VictoryAxis dependentAxis
                        offsetX={10}
                        orientation="right"
                        crossAxis={false}
                        style={{
                            tickLabels: { fill: 'grey' },
                            axis: { stroke: '#ffffff' }
                        }}
                    />
                    <VictoryGroup offset={0}>

                        <VictoryBar
                            data={[{ x: "Sun", y: 30 }]}
                            style={{ data: { fill: "#9dcbf5" } }}
                            cornerRadius={3}

                        />
                        <VictoryBar
                            data={[{ x: "Mon", y: 65 }]}
                            style={{ data: { fill: "#9dcbf5" } }}
                            cornerRadius={3}

                        />
                        <VictoryBar
                            data={[{ x: "Tue", y: 14 }]}
                            style={{ data: { fill: "#9dcbf5" } }}
                            cornerRadius={3}

                        />
                        <VictoryBar

                            labelComponent={
                                <VictoryLabel

                                    dy={-25}
                                    backgroundStyle={{ fill: "white" }}
                                    backgroundPadding={{ bottom: 10, top: 10, left: 10, right: 10 }}
                                />
                            }
                            labels={['↑25%']}
                            data={[{ x: "Wed", y: 90 }]}
                            style={{
                                data: { fill: "#1d8aef" },
                                labels: {
                                    fill: "#3bca38",
                                    fontSize: 16,
                                    fontWeight: "bold",

                                },

                            }}
                            cornerRadius={3}


                        />
                        <VictoryBar
                            data={[{ x: "Thu", y: 61, }]}
                            style={{ data: { fill: "#9dcbf5" } }}
                            cornerRadius={3}

                        />
                        <VictoryBar

                            data={[{ x: "Fri", y: 14 }]}
                            style={{
                                data: { fill: "#9dcbf5" },

                            }}
                            cornerRadius={3}

                            barWidth={20}

                        />
                        <VictoryBar
                            data={[{ x: "Sat", y: 33 }]}
                            style={{ data: { fill: "#9dcbf5" } }}
                            cornerRadius={3}

                        />
                    </VictoryGroup>
                </VictoryChart>
            </View>
        </View>
    );
};

export default BarChart;
