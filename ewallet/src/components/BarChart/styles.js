import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 220,
        width: '100%',
        backgroundColor: 'yellow',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    subContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white"
    },
    titleStyle: {
        fontSize: 20,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    }

})

export default styles;