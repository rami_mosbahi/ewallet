import React from 'react';
import {
    View,
    Modal,
    Image,
    Text,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import ModalPoup from '../common/AppAlert/ModalPoup'
import successLogo from '../../assets/img/success.png'
import warningLogo from '../../assets/img/alert.png'
import closeLogo from '../../assets/img/x.png'
import styles from './styles';

import Form from './Form'

const FormCreditCard = ({ visible, type, msg, close }) => {
    return (
        <ModalPoup visible={visible}>
            <ScrollView>
                <View style={{ alignItems: 'center' }}>
                    <View style={styles.header}>
                        <TouchableOpacity onPress={() => { close() }}>
                            <Image
                                source={closeLogo}
                                style={{ height: 30, width: 30 }}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text>This is the form</Text>
                </View>
                <Form />
            </ScrollView>



        </ModalPoup>
    );
};



export default FormCreditCard;