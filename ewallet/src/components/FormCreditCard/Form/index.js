import React from 'react';
import { Text, Button, View } from 'react-native';
import Auxilary from '../../../hoc/Auxilary'
import Input from '../../common/Input'

const Form = () => {
    return (
        <Auxilary>

            <Input
                error={false}
                onChangeText={(value) => {
                    // onChangeText({ name: 'lastName', value: value });
                }}
                value={''}
                label="Last name"
                placeholder="Enter Last name "
            />
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row'
                }}
            >
            </View>

        </Auxilary>
    );
};

export default Form;