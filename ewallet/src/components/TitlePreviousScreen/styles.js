import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginVertical: 20,
        backgroundColor: '#fafafa'
    },
    titleStyle: {
        fontSize: 24,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700',
        paddingLeft: 14,
    },
    previousImageStyle: {
        width: windowWidth / 21,
        height: windowWidth / 21,
        borderRadius: 10,
        resizeMode: "cover",
    }

})

export default styles;