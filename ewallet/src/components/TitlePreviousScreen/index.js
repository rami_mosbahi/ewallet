
import React, { useEffect } from 'react';
import { View, Text, Image, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles'
import previousIcon from '../../assets/img/back.png'



const TitlePreviousScreen = ({ title, press }) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={() => {
                    press()
                }}
            >
                <Image
                    style={styles.previousImageStyle}
                    source={previousIcon}
                />
            </TouchableOpacity>
            <Text style={styles.titleStyle}>{title}</Text>
        </View>
    );
};

export default TitlePreviousScreen;
