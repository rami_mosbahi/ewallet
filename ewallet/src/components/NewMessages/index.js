
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'
import Button from '../../components/common/Button'


const NewMessages = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.titlePart}>
                <Text style={styles.titleStyle}>New Messages!</Text>
                <Text style={styles.subTitleStyle}>Open for more detail</Text>
            </View>
            <View style={styles.buttonPart}>
                <Button
                    width={"65%"}
                    color={"#1d8aef"}
                    borderColor={"#1d8aef"}
                    title={"Open"}
                    titleColor={"white"}
                    press={() => {

                    }}
                />
            </View>


        </View>
    );
};

export default NewMessages;
