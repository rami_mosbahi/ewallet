import React from 'react';
import { View, TextInput } from 'react-native';
import colors from '../../../assets/theme/colors';
import styles from './styles';

const Input = ({
    onChangeText,
    iconPosition,
    icon,
    style,
    value,
    label,
    error,
    type,
    maxLength,
    textAlign,

    ...props
}) => {
    const [focused, setFocused] = React.useState(false);

    const getFlexDirection = () => {
        if (icon && iconPosition) {
            if (iconPosition === 'left') {
                return 'row';
            } else if (iconPosition === 'right') {
                return 'row-reverse';
            }
        }
    };

    const getBorderColor = () => {
        if (error) {
            return colors.danger;
        }

        if (focused) {
            return colors.primary;
        } else {
            return colors.grey;
        }
    };
    return (
        <View style={styles.inputContainer}>
            <View
                style={[
                    styles.wrapper,
                    { alignItems: icon ? 'center' : 'baseline' },
                    { borderColor: getBorderColor(), flexDirection: getFlexDirection() },
                ]}>
                <View>{icon && icon}</View>

                <TextInput

                    keyboardType={type}
                    maxLength={maxLength}
                    style={[styles.textInput, style]}
                    onChangeText={onChangeText}
                    value={value}
                    onFocus={() => {
                        setFocused(true);
                    }}
                    onBlur={() => {
                        setFocused(false);
                    }}
                    {...props}

                />
            </View>


        </View>
    );
};

export default Input;