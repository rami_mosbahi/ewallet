import React from 'react';
import {
    View,
    Modal,
    Image,
    Text,
    TouchableOpacity,
    Animated,
} from 'react-native';
import ModalPoup from './ModalPoup'
import successLogo from '../../../assets/img/success.png'
import warningLogo from '../../../assets/img/alert.png'
import closeLogo from '../../../assets/img/x.png'
import styles from './styles';
import Button from '../Button'


const AppAlert = ({ visible, type, msg, close }) => {
    return (
        <ModalPoup visible={visible}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => { close() }}>
                        <Image
                            source={closeLogo}
                            style={styles.imgStyle}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ alignItems: 'center' }}>
                <Image
                    source={type == "success" ? successLogo : warningLogo}
                    style={styles.successImg}
                />
            </View>

            <Text style={styles.msgAlert}>
                {msg}
            </Text>
            <Button
                color={"#1d8aef"}
                borderColor={"#1d8aef"}
                title={"Okey"}
                titleColor={"white"}
                press={() => {
                    close()
                }}
            />
        </ModalPoup>
    );
};



export default AppAlert;