import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
    modalBackGround: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        width: '80%',
        backgroundColor: 'white',
        paddingHorizontal: 20,
        paddingVertical: 30,
        borderRadius: 20,
        elevation: 20,
    },
    header: {
        width: '100%',
        height: 40,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    container: {
        alignItems: 'center'
    },
    imgStyle: {
        height: 30,
        width: 30
    },
    msgAlert: {
        marginVertical: 30,
        fontSize: 20,
        textAlign: 'center'
    },
    successImg: {
        height: 150,
        width: 150,
        marginVertical: 10
    },
})

export default styles;