import React from 'react'
import {
    StyleSheet, Text, View,
    TouchableOpacity, Dimensions,
    ScrollView
} from 'react-native'
const OPTIONS = ['red', 'blue', 'yellow', 'green', 'orange', 'white']
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
import styles from './styles'

const ModalPicker = (props) => {
    const OPTIONS = props.options

    const onPressItem = (option) => {
        props.changeModalVisible(false)
        props.setData(option)

    }
    const option = OPTIONS.map((item, index) => {
        return (
            <TouchableOpacity
                style={styles.option}
                key={index}
                onPress={() => {
                    onPressItem(item)
                }}
            >
                <Text style={styles.text}>
                    {item}
                </Text>

            </TouchableOpacity>
        )

    })
    return (
        <TouchableOpacity
            onPress={() => props.changeModalVisible(false)}
            style={styles.container}
        >
            <View
                style={[styles.modal, { width: WIDTH - 20, height: HEIGHT / 2 }]}
            >
                <ScrollView>
                    {option}
                </ScrollView>
            </View>
        </TouchableOpacity>
    )
}




export default ModalPicker;