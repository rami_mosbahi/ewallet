import React from 'react';
import {
    Text,
    TouchableOpacity,
} from 'react-native';
import styles from './styles';


const Button = ({ title, color, borderColor, titleColor, press, width }) => {
    return (
        <TouchableOpacity
            style={{
                backgroundColor: color,
                height: 40,
                width: width,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 7,
                marginBottom: 5,
                borderColor: borderColor,
                borderWidth: 1,

            }}
            onPress={() => {
                press()
            }}>
            <Text style={{ fontSize: 15, textAlign: 'center', color: titleColor }}>
                {title}
            </Text>
        </TouchableOpacity>
    );
};



export default Button;