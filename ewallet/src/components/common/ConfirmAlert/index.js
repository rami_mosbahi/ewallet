import React from 'react';
import {
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';
import ModalPoup from './ModalPoup'
import closeLogo from '../../../assets/img/x.png'
import styles from './styles';
import Button from '../Button'


const ConfirmAlert = ({ visible, msg, close, confirm, cancel }) => {
    return (
        <ModalPoup visible={visible}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => { close() }}>
                        <Image
                            source={closeLogo}
                            style={styles.imgStyle}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container}>
                <Text style={styles.txtConfirmStyle}>
                    Confirm dialog
            </Text>
            </View>

            <Text style={styles.txtmsgStyle}>
                {msg}
            </Text>
            <Button
                color={"#1d8aef"}
                borderColor={"#1d8aef"}
                title={"Okey"}
                titleColor={"white"}
                press={() => {
                    confirm()
                }}
            />
            <Button
                color={"white"}
                borderColor={"#1d8aef"}
                title={"Cancel"}
                titleColor={"#1d8aef"}
                press={() => {
                    cancel()
                }}
            />
        </ModalPoup>
    );
};



export default ConfirmAlert;