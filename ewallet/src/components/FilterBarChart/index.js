/* eslint-disable prettier/prettier */
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles'


const FilterBarChart = () => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.ButtonActive}>
                <Text
                    style={styles.titleButtonActive}
                >Weekly</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.ButtonInActive}>
                <Text
                    style={styles.titleButtonInActive}
                >Monthly</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.ButtonInActive}>
                <Text
                    style={styles.titleButtonInActive}
                >Yearly</Text>
            </TouchableOpacity>

        </View>
    );
};

export default FilterBarChart;
