/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles'

const TransactionItem = ({ img, name, type, price, press }) => {
    return (
        <TouchableOpacity style={styles.container}
            onPress={() => { press() }}
        >
            <View
                style={styles.customerPart}
            >
                <Image
                    style={styles.customerImage}
                    source={{
                        uri: img,
                    }} />
                <View
                    style={styles.customerDetailsPart}
                >
                    <Text
                        style={styles.customerDetailsNamePart}
                    >{name}</Text>
                    <Text
                        style={styles.customerDetailsTypePart}
                    >{type}</Text>
                </View>
            </View>
            <View
                style={styles.amountPart}
            >
                <Text style={styles.amountTextStyle}>{price}</Text>

            </View>
        </TouchableOpacity>
    );
};

export default TransactionItem;
