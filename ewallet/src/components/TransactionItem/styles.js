import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        height: 65,
        width: '100%',
        borderRadius: 10,
        flexDirection: 'row',
        marginBottom: 5,
        shadowColor: '#b3b3b3',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 0.2,

    },
    customerPart: {
        height: "100%",
        width: '50%',
        backgroundColor: 'white',
        borderTopStartRadius: 10,
        borderBottomStartRadius: 10,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 10
    },
    customerDetailsPart: {
        height: "100%",
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 10
    },
    amountPart: {
        height: "100%",
        width: '50.1%',
        backgroundColor: 'white',
        borderTopEndRadius: 10,
        borderBottomEndRadius: 10,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',

    },
    amountTextStyle: {
        fontSize: 17,
        color: '#1d8aef',
        fontFamily: 'notoserif',
        fontWeight: '700',
        paddingRight: 15
    },

    customerImage: {
        width: "30%",
        height: "70%",
        borderRadius: 10,
        resizeMode: "cover",
    },
    customerDetailsNamePart: {
        fontSize: 12,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    customerDetailsTypePart: {
        fontSize: 12,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '400'
    }

})

export default styles;