import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    LinearGradientStyle: {
        width: "100%",
        height: 189,
        borderRadius: 15,
        flexDirection: 'row',
        padding: 22,
        marginBottom: 5
    },
    container1: {
        width: '70%',
        alignItems: 'flex-start',
    },
    styleBalance: {
        fontSize: 15,
        color: '#fff',
        fontFamily: 'Lato-Regular',
        fontWeight: '600'
    },
    styleBalanceValue: {
        fontSize: 32, color: '#fff',
        fontFamily: 'Lato-Medium',
        fontWeight: '700'
    },
    styleCreditCardNumber: {
        marginTop: 5,
        fontSize: 20,
        color: '#fff',
        fontFamily: 'Lato-Regular',
        fontWeight: '700',
        letterSpacing: 5
    },
    styleHolderName: {
        fontSize: 15,
        color: '#fff',
        fontFamily: 'Lato-Regular',
        fontWeight: '600',
    },
    styleRightPart: {
        width: '30%',
        alignItems: 'flex-end',
    },


})

export default styles;