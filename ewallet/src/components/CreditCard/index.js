/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles'


const CreditCard = ({ navigationTo, data }) => {
    const onPress = () => {
        navigationTo()
    }
    return (
        <TouchableOpacity
            activeOpacity={.99}
            onPress={onPress}
        >
            <LinearGradient
                start={{ x: 0, y: 1 }}
                end={{ x: 1, y: 1 }}
                colors={['#3995ea', '#59a7ee', '#73b5f2']}
                style={styles.LinearGradientStyle}
            >
                <View style={styles.container1}>
                    <Text
                        style={styles.styleBalance}
                    >
                        Balance
</Text>
                    <Text
                        style={styles.styleBalanceValue}
                    >
                        ${data.balance}
                    </Text>

                    <Text
                        style={styles.styleCreditCardNumber}
                    >
                        •••• •••• {data.id[8] + data.id[9] + data.id[10] + data.id[11]}
                    </Text>
                    <Text
                        style={{
                            marginTop: 5,
                            fontSize: 12,
                            color: '#fff',
                            fontFamily: 'Lato-Regular',
                            fontWeight: '600',

                        }}
                    >
                        Name
                    </Text>
                    <Text
                        style={styles.styleHolderName}>
                        {data.holdername}
                    </Text>
                </View>
                <View style={styles.styleRightPart}>
                </View>
            </LinearGradient>

        </TouchableOpacity>
    );
};

export default CreditCard;
