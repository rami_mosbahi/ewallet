import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        backgroundColor: '#f5f5f5',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        borderRadius: 8
    },
    ButtonActive: {
        backgroundColor: 'white',
        width: '40%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        borderRadius: 10,
        elevation: 1
    },
    ButtonInActive: {
        backgroundColor: '#f5f5f5',
        width: '40%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        borderRadius: 10
    },
    titleButtonActive: {
        fontSize: 13,
        color: '#1d8aef',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    titleButtonInActive: {
        fontSize: 13,
        color: '#898989',
        fontFamily: 'notoserif',
        fontWeight: '700'
    }


})

export default styles;