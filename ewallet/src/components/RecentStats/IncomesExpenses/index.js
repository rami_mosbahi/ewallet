/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'
import { expenseActions } from '../../../store/reducers/expense-slice'
import { useSelector, useDispatch } from 'react-redux'


const IncomesExpenses = () => {
    const dispatch = useDispatch()
    const expenseDashboardisActive = useSelector((state) => state.expense.expenseDashboardisActive);

    return (
        <View style={styles.container}>
            <TouchableOpacity style={!expenseDashboardisActive ? styles.ButtonActive : styles.ButtonInActive}
                onPress={() => {
                    dispatch(expenseActions.setExpenseDashboardisActive({ expenseDashboardisActive: false }))
                }}>
                <Text
                    style={!expenseDashboardisActive ? styles.titleButtonActive : styles.titleButtonInActive}
                >Income</Text>
            </TouchableOpacity>
            <TouchableOpacity style={expenseDashboardisActive ? styles.ButtonActive : styles.ButtonInActive} onPress={() => {
                dispatch(expenseActions.setExpenseDashboardisActive({ expenseDashboardisActive: true }))
            }}>
                <Text
                    style={expenseDashboardisActive ? styles.titleButtonActive : styles.titleButtonInActive}
                >Expense</Text>
            </TouchableOpacity>

        </View >
    );
};

export default IncomesExpenses;
