import React, { useEffect } from 'react';
import {
    View,
    StyleSheet,
    Button,
    Modal,
    Image,
    Text,
    TouchableOpacity,
    Animated,
    ActivityIndicator,
} from 'react-native';
import { VictoryBar, VictoryGroup, VictoryChart, VictoryAxis, VictoryLabel, VictoryScatter, VictoryArea } from "victory-native";
import Svg, {
    Circle,
    Ellipse,
    G,
    TSpan,
    TextPath,
    Path,
    Polygon,
    Polyline,
    Line,
    Rect,
    Use,
    Symbol,
    Defs,
    LinearGradient,
    RadialGradient,
    Stop,
    ClipPath,
    Pattern,
    Mask,
} from 'react-native-svg';
const CustomLabel = props => <Text>{props}</Text>;


import styles from './styles'
import { useSelector, useDispatch } from 'react-redux'
import moment from 'moment'
import { expenseActions } from '../../../store/reducers/expense-slice'
import { incomeActions } from '../../../store/reducers/income-slice'
import { getAllExpenses } from '../../../store/actions/expense-action'
import { getAllIncomes } from '../../../store/actions/income-action'

const AreaChart = ({ data }) => {
    const AllExpeneses = useSelector((state) => state.expense.expenses);
    const AllIncomes = useSelector((state) => state.income.incomes);
    const expensesForChart = useSelector((state) => state.expense.expensesForChart);
    const incomesForChart = useSelector((state) => state.income.incomesForChart);
    const expenseDashboardisActive = useSelector((state) => state.expense.expenseDashboardisActive);

    const dispatch = useDispatch();


    // Incomes
    const dataForCharIncome = () => {
        const FilteredIncomes = AllIncomes.filter(el => el.destinationCard == data.id)
        var i = 0;
        var month, amount
        var newArray = []
        for (; i < FilteredIncomes.length; i++) {
            month = FilteredIncomes[i]['date'];
            amount = FilteredIncomes[i]['amount'];
            newArray.push({ month: moment(month).format('MMM'), amount: parseInt(amount) })
        }


        var l = (groupeBy(newArray).length) - (parseInt(moment().format('MM')))
        var extractedArray = groupeBy(newArray).slice(0, -l)



        dispatch(incomeActions.setIncomesForChart({
            incomesForChart: extractedArray
        }))



    }


    // Expenses
    const dataForCharExpense = () => {
        const FilteredExpenses = AllExpeneses.filter(el => el.from == data.id)
        var i = 0;
        var month, amount
        var newArray = []
        for (; i < FilteredExpenses.length; i++) {
            month = FilteredExpenses[i]['date'];
            amount = FilteredExpenses[i]['amount'];
            newArray.push({ month: moment(month).format('MMM'), amount: parseInt(amount) })
        }


        var l = (groupeBy(newArray).length) - (parseInt(moment().format('MM')))
        var extractedArray = groupeBy(newArray).slice(0, -l)



        dispatch(expenseActions.setExpensesForChart({
            expensesForChart: extractedArray
        }))


    }
    const groupeBy = (array) => {


        var traveler = array;

        Array.prototype.sum = function (prop) {
            var total = 0
            for (var i = 0, _len = this.length; i < _len; i++) {
                total += this[i][prop]
            }
            return total
        }



        var jan = array.filter(el => el.month == 'Jan').sum("amount")

        var feb = array.filter(el => el.month == 'Feb').sum("amount")
        var mar = array.filter(el => el.month == 'Mar').sum("amount")
        var apr = array.filter(el => el.month == 'Apr').sum("amount")
        var may = array.filter(el => el.month == 'May').sum("amount")
        var jun = array.filter(el => el.month == 'Jun').sum("amount")
        var jul = array.filter(el => el.month == 'Jul').sum("amount")
        var aug = array.filter(el => el.month == 'Aug').sum("amount")
        var sep = array.filter(el => el.month == 'Sep').sum("amount")
        var oct = array.filter(el => el.month == 'Oct').sum("amount")
        var nov = array.filter(el => el.month == 'Nov').sum("amount")
        var dec = array.filter(el => el.month == 'Dec').sum("amount")

        return [
            { x: 'Jan', y: jan },
            { x: 'Feb', y: feb },
            { x: 'Mar', y: mar },
            { x: 'Apr', y: apr },
            { x: 'May', y: may },
            { x: 'Jun', y: jun },
            { x: 'Jul', y: jul },
            { x: 'Aug', y: aug },
            { x: 'Sep', y: sep },
            { x: 'Oct', y: oct },
            { x: 'Nov', y: nov },
            { x: 'Dec', y: dec },
        ]
    }


    useEffect(() => {
        if (AllIncomes.length == 0)
            return;

        dataForCharIncome()
    }, [AllIncomes])



    useEffect(() => {
        if (AllExpeneses.length == 0)
            return;

        dataForCharExpense()

    }, [AllExpeneses])


    useEffect(() => {
        if (AllIncomes.length == 0)
            dispatch(incomeActions.setIncomesForChart({
                incomesForChart: [{ x: "0", y: "0" }]
            }))
    }, [AllIncomes])



    useEffect(() => {
        if (AllExpeneses.length == 0)
            dispatch(expenseActions.setExpensesForChart({
                expensesForChart: [{ x: "0", y: "0" }]
            }))

    }, [AllExpeneses])





    useEffect(() => {


        if (expenseDashboardisActive) {
            dispatch(getAllExpenses())
        }
        else {
            dispatch(getAllIncomes())
        }

    }, [expenseDashboardisActive])






    return (
        <View style={styles.container}>
            <View style={{

                justifyContent: "center",
                alignItems: 'flex-start',


            }}>

                <VictoryChart
                    width={350}
                    height={150}
                >
                    <Defs>
                        <LinearGradient id="gradientStroke"
                            x1="0%"
                            x2="0%"
                            y1="0%"
                            y2="100%"
                        >
                            <Stop offset="0%" stopColor="#1E93FA" stopOpacity="0.3" />
                            <Stop offset="100%" stopColor="#1E93FA" stopOpacity="0" />
                        </LinearGradient>
                    </Defs>
                    <VictoryAxis
                        key={Math.random()}
                        style={{
                            tickLabels: { fill: 'grey' },
                            axis: { stroke: '#ffffff' }
                        }}

                    />

                    <VictoryGroup offset={0}>
                        <VictoryArea
                            key={Math.random()}
                            style={{
                                data: {
                                    fill: 'url(#gradientStroke)',
                                    fillOpacity: 0.7, stroke: "#1E93FA", strokeWidth: 3
                                },
                                labels: {
                                    fontSize: 15,
                                    fill: ({ datum }) => datum.x === 3 ? "#000000" : "#c43a31"
                                }
                            }}
                            data={expenseDashboardisActive ? expensesForChart : incomesForChart}

                        />
                        <VictoryScatter
                            style={{
                                data: {
                                    fill: ({ datum }) => datum.x === 3 ? "#000000" : "#1E93FA",
                                    stroke: ({ datum }) => datum.x === 3 ? "#000000" : "white",
                                    fillOpacity: 0.7,
                                    strokeWidth: 2
                                },
                            }}
                            size={5}
                            data={[

                            ]}
                            size={5}
                        />
                    </VictoryGroup>
                </VictoryChart>


            </View>
        </View>
    );
};

export default AreaChart;
