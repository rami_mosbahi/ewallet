import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 120,
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    titleStyle: {
        fontSize: 20,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    }

})

export default styles;