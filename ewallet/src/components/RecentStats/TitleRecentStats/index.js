
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'



const TitleRecentStats = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleStyle}>Recent Stats</Text>
        </View>
    );
};

export default TitleRecentStats;
