import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 10
    },
    titlePart: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        backgroundColor: 'white',
        height: '100%',
        width: '50%'


    },
    buttonPart: {
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-end',
        height: '100%',
        width: '50%'

    },
    titleStyle: {
        fontSize: 18,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    },
    subTitleStyle: {
        fontSize: 15,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '400'
    }

})

export default styles;