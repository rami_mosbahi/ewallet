
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'
import Button from '../../../components/common/Button'


const ViewStatistics = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.titlePart}>
                <Text style={styles.titleStyle}>View Statistics</Text>
                <Text style={styles.subTitleStyle}>Open for more detail</Text>
            </View>
            <View style={styles.buttonPart}>
                <Button
                    width={"65%"}
                    color={"#1d8aef"}
                    borderColor={"#1d8aef"}
                    title={"Open"}
                    titleColor={"white"}
                    press={() => {
                        navigation.navigate("Statistics");
                    }}
                />
            </View>


        </View>
    );
};

export default ViewStatistics;
