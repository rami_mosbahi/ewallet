/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './styles'
import TitleRecentStats from './TitleRecentStats'
import IncomesExpenses from './IncomesExpenses'
import AreaChart from './AreaChart'
import ViewStatistics from './ViewStatistics'






const RecentStats = ({ navigation, data }) => {


    return (
        <View style={styles.container}>
            <TitleRecentStats />
            <IncomesExpenses />
            <AreaChart data={data} />

            <ViewStatistics navigation={navigation} />

        </View>
    );
};

export default RecentStats;
