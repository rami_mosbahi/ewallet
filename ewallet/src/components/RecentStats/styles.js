import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        height: 305,
        width: '100%',
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 15,
        shadowColor: '#b3b3b3',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 4,
        elevation: 12,

    },

})

export default styles;