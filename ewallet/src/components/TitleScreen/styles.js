import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;


const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        backgroundColor: '#fafafa',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginBottom: 10
    },
    titleStyle: {
        fontSize: 20,
        color: '#323232',
        fontFamily: 'notoserif',
        fontWeight: '700'
    }

})

export default styles;