
import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';
import styles from './styles'



const TitleScreen = ({ title }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.titleStyle}>{title}</Text>
        </View>
    );
};

export default TitleScreen;
