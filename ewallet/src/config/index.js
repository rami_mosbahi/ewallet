import { PROD_BACKEND_URL, DEV_BACKEND_URL } from '@env'

const devEnv = {
    BACKEND_URL: DEV_BACKEND_URL
}

const prodEnv = {
    BACKEND_URL: PROD_BACKEND_URL
}

export default __DEV__ ? devEnv : prodEnv;