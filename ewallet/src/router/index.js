import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";

import AllCardsScreen from "../screens/AllCardsScreen";
import DashboardScreen from "../screens/DashboardScreen";
import StatisticScreen from "../screens/StatisticScreen";
import FormCardScreen from "../screens/FormCardScreen";
import AccountScreen from "../screens/AccountScreen";
import TransferScreen from "../screens/TransferScreen";




const RootStack = createStackNavigator();
const horizontalAnimation = {
    gestureDirection: 'horizontal',
    cardStyleInterpolator: ({ current, layouts }) => {
        return {
            cardStyle: {
                transform: [
                    {
                        translateX: current.progress.interpolate({
                            inputRange: [0, 1],
                            outputRange: [layouts.screen.width, 0],
                        }),
                    },
                ],
            },
        };
    },
};
const Router = () => (

    <RootStack.Navigator
        screenOptions={horizontalAnimation}
    >
        <RootStack.Screen
            name={"AccountScreen"}
            component={AccountScreen}
            options={{
                headerShown: false
            }}
        />
        <RootStack.Screen
            name={"Transfer"}
            component={TransferScreen}
            options={{
                headerShown: false
            }}
        />
        <RootStack.Screen
            name={"AllCards"}
            component={AllCardsScreen}
            options={{
                headerShown: false
            }}
        />
        <RootStack.Screen
            name="Dashboard"
            component={DashboardScreen}
            options={{
                headerShown: false,
            }}
        />

        <RootStack.Screen
            name="Statistics"
            component={StatisticScreen}
            options={{
                headerShown: false,
            }}
        />
        <RootStack.Screen
            name="FormCard"
            component={FormCardScreen}
            options={{
                headerShown: false,
            }}
        />
    </RootStack.Navigator>
);

export default Router;