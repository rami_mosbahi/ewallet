import { createSlice } from '@reduxjs/toolkit';

const postSlice = createSlice({
    name: 'post',
    initialState: { posts: [], loadingPost: false },
    reducers: {
        getPosts(state, action) {
            state.posts = action.payload.posts;
        },
        showLoadingPosts(state, action) {
            state.loadingPost = action.payload.loadingPost;
        },
    },
});

export const postActions = postSlice.actions;

export default postSlice;