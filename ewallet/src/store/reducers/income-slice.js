import { createSlice } from '@reduxjs/toolkit';

const incomeSlice = createSlice({
    name: 'income',
    initialState: {
        incomes: [],
        incomesForChart: [],

    },
    reducers: {
        setIncomesForChart(state, action) {
            state.incomesForChart = action.payload.incomesForChart
        },
        setIncomes(state, action) {

            state.incomes = action.payload.incomes
        },
        replaceCart(state, action) {
            state.totalQuantity = action.payload.totalQuantity;
            state.items = action.payload.items;
        },

    },
});

export const incomeActions = incomeSlice.actions;

export default incomeSlice;