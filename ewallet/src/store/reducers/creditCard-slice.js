import { createSlice } from '@reduxjs/toolkit';

const creditCardSlice = createSlice({
    name: 'creditCard',
    initialState: {
        creditCards: [],
        allCreditCards: [],
        loadingCreditCards: false,
        errorCreditCards: false,
        errorCreditCardsMsg: '',
        errorToAddCard: false,
        typeCreditCardSelected: 'Type',
        numberCreditCardSelected: '',
        holderNameCreditCardSelected: '',
        mmCreditCardSelected: 'MM',
        yyCreditCardSelected: 'YY',
        cvvCreditCardSelected: '',
        errorFormValidation: false,
        errorFormValidationMsg: '',
        showValidFormValidation: false,
        currentCreditCard: 'current now'

    },
    reducers: {
        setAllCreditCards(state, action) {
            state.allCreditCards = action.payload.allCreditCards
        },
        updateBalance(state, action) {
            var tab = state.creditCards
            for (var i in tab) {
                if (tab[i].id == action.payload.id) {
                    tab[i].balance = action.payload.balance;
                    break; //Stop this loop, we found it!
                }
            }
            state.creditCards = tab

        },
        setCurrentCreditCard(state, action) {
            state.currentCreditCard = action.payload.currentCreditCard
        },
        resetAllCards(state, action) {
            state.creditCards = []
        },
        resetSelectedForm(state, action) {
            state.typeCreditCardSelected = 'Type';
            state.numberCreditCardSelected = '';
            state.holderNameCreditCardSelected = '';
            state.mmCreditCardSelected = 'MM';
            state.yyCreditCardSelected = 'YY';
            state.cvvCreditCardSelected = '';
        },
        setShowValidFormValidation(state, action) {
            state.showValidFormValidation = action.payload.showValidFormValidation;
        },
        setErrorFormValidation(state, action) {
            state.errorFormValidation = action.payload.errorFormValidation;
            state.errorFormValidationMsg = action.payload.errorFormValidationMsg;
        },
        getCreditCards(state, action) {

            state.creditCards = action.payload.creditCards;
        },
        showLoadingCreditCards(state, action) {
            state.loadingCreditCards = action.payload.loadingCreditCards;
        },
        setErrorCreditCards(state, action) {
            state.errorCreditCards = action.payload.errorCreditCards;
            state.errorCreditCardsMsg = action.payload.errorCreditCardsMsg;
        },
        removeCreditCards(state, action) {
            let filteredArray = state.creditCards.filter(el => el.id != action.payload.id);
            state.creditCards = filteredArray;
        },
        setErrorToAddCard(state, action) {
            state.errorToAddCard = action.payload.errorToAddCard;
        },
        setTypeCreditCardSelected(state, action) {
            state.typeCreditCardSelected = action.payload.typeCreditCardSelected;
        },
        setNumberCreditCardSelected(state, action) {
            state.numberCreditCardSelected = action.payload.numberCreditCardSelected;
        },
        setHolderNameCreditCardSelected(state, action) {
            state.holderNameCreditCardSelected = action.payload.holderNameCreditCardSelected;
        },
        setMmCreditCardSelected(state, action) {
            state.mmCreditCardSelected = action.payload.mmCreditCardSelected;
        },
        setYyCreditCardSelected(state, action) {
            state.yyCreditCardSelected = action.payload.yyCreditCardSelected;
        },
        setCvvCreditCardSelected(state, action) {
            state.cvvCreditCardSelected = action.payload.cvvCreditCardSelected;
        },
        addCreditCard(state, action) {
            state.creditCards = [...state.creditCards, action.payload.creditCard]

        },


    },
});

export const creditCardActions = creditCardSlice.actions;

export default creditCardSlice;