import { createSlice } from '@reduxjs/toolkit';

const expenseSlice = createSlice({
    name: 'income',
    initialState: {
        expenses: [],
        expensesForChart: [],
        expenseDashboardisActive: false,
        destinationSelected: 'Destination',
        amountToTransferSelected: "",
        typeSelected: '',
        destinationCardSelected: '',
        errorTransferFormValidation: false,
        showValidTransferFormValidation: false,
        errorTransferFormValidationMsg: '',



    },
    reducers: {
        setExpenseDashboardisActive(state, action) {
            state.expenseDashboardisActive = action.payload.expenseDashboardisActive
        },
        setExpensesForChart(state, action) {
            state.expensesForChart = action.payload.expensesForChart
        },
        setExpenses(state, action) {
            state.expenses = action.payload.expenses
        },
        setDestinationCard(state, action) {
            state.destinationCardSelected = action.payload.destinationCardSelected;
        },
        resetSelectedForm(state, action) {
            state.destinationSelected = 'Destination';
            state.amountToTransferSelected = '';
            state.typeSelected = '';
        },
        addExpense(state, action) {
            // state.errorTransferFormValidationMsg = action.payload.errorTransferFormValidationMsg;
        },
        setErrorTransferFormValidationMsg(state, action) {
            state.errorTransferFormValidationMsg = action.payload.errorTransferFormValidationMsg;
        },
        setErrorTransferFormValidation(state, action) {
            state.errorTransferFormValidation = action.payload.errorTransferFormValidation;
            state.errorTransferFormValidationMsg = action.payload.errorTransferFormValidationMsg;
        },
        setShowValidTransferFormValidation(state, action) {
            state.showValidTransferFormValidation = action.payload.showValidTransferFormValidation;
        },
        setDestinationSelected(state, action) {
            state.destinationSelected = action.payload.destinationSelected;
        },
        setAmountToTransferSelected(state, action) {
            state.amountToTransferSelected = action.payload.amountToTransferSelected;
        },
        setTypeSelected(state, action) {
            state.typeSelected = action.payload.typeSelected;
        },
    },
});

export const expenseActions = expenseSlice.actions;

export default expenseSlice;