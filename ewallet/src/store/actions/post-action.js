
import { uiActions } from '../reducers/ui-slice';
import { postActions } from '../reducers/post-slice';

export const fetchPostData = () => {
    return async (dispatch) => {
        dispatch(
            postActions.showLoadingPosts({ loadingPost: true })
        );
        const fetchData = async () => {
            const response = await fetch(
                'http://192.168.1.7:3000/cards'
            );

            if (!response.ok) {
                throw new Error('Could not fetch cart data!');
            }

            const data = await response.json();

            return data;
        };

        try {
            const postData = await fetchData();
            dispatch(
                postActions.getPosts({
                    posts: postData || [],
                })
            );
            dispatch(
                postActions.showLoadingPosts({ loadingPost: false })
            );
        } catch (error) {
            dispatch(
                uiActions.showNotification({
                    status: 'error',
                    title: 'Error!',
                    message: 'Fetching cart data failed!',
                })
            );
        }
    };
};

