
import { creditCardActions } from '../reducers/creditCard-slice';
import env from "../../config"



export const setCurrentCreditCardAction = (currentCreditCard) => {
    return async (dispatch) => {
        dispatch(
            creditCardActions.setCurrentCreditCard({ currentCreditCard: currentCreditCard })
        );
    };
};




export const getAllCreditCards = () => {
    return async (dispatch) => {


        const fetchData = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards`
            );

            if (!response.ok) {
                throw new Error('Could not fetch cart data!');

            }

            const data = await response.json();

            return data;
        };

        try {
            const getAllCreditCards = await fetchData();
            dispatch(
                creditCardActions.setAllCreditCards({
                    allCreditCards: getAllCreditCards || [],
                })
            );

        } catch (error) {

        }
    };
};



export const fetchCreditCardData = (id) => {
    return async (dispatch) => {
        dispatch(
            creditCardActions.showLoadingCreditCards({ loadingCreditCards: true })
        );

        const fetchData = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards`
            );

            if (!response.ok) {
                throw new Error('Could not fetch cart data!');

            }

            const data = await response.json();

            return data;
        };

        try {
            const credtCardsData = await fetchData();
            let filteredArray = credtCardsData.filter(el => el.customer.id == id);
            dispatch(
                creditCardActions.getCreditCards({
                    creditCards: filteredArray || [],
                })
            );
            dispatch(
                creditCardActions.showLoadingCreditCards({ loadingCreditCards: false })
            );
        } catch (error) {
            dispatch(
                creditCardActions.getCreditCards({
                    creditCards: [],
                })
            );
            dispatch(
                creditCardActions.setErrorCreditCards({
                    errorCreditCards: true,
                    errorCreditCardsMsg: "Please check your connection !"

                }),
            )
            dispatch(
                creditCardActions.showLoadingCreditCards({ loadingCreditCards: false })
            );

        }
    };
};

export const deleteCreditCard = (idCreditCard) => {
    return async (dispatch) => {
        dispatch(
            creditCardActions.showLoadingCreditCards({ loadingCreditCards: true })
        );

        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards/${idCreditCard}`,
                {
                    method: 'DELETE',
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');

            }
        };

        try {
            await sendRequest().then(() => {
                dispatch(
                    creditCardActions.removeCreditCards({ id: idCreditCard })
                );
            });

            dispatch(
                creditCardActions.showLoadingCreditCards({ loadingCreditCards: false })
            );
        } catch (error) {

            dispatch(
                creditCardActions.setErrorCreditCards({
                    errorCreditCards: true,
                    errorCreditCardsMsg: "Please check your connection !"
                }),
            )
            dispatch(
                creditCardActions.showLoadingCreditCards({ loadingCreditCards: false })
            );
        }
    };
};




export const addCreditCard = (creditCard) => {
    return async (dispatch) => {


        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: creditCard.number,
                        number: creditCard.number,
                        holdername: creditCard.holderName,
                        type: creditCard.type,
                        cvv: creditCard.cvv,
                        expirationMonth: creditCard.expirationMonth,
                        expirationYear: creditCard.expirationYear,
                        balance: 1000,
                        customer: {
                            id: creditCard.account.id,
                            name: creditCard.account.name,
                            address: "UFC",
                            country: "USA",
                        }
                    })
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');
            }
        };

        try {
            dispatch(
                creditCardActions.addCreditCard({
                    creditCard: {
                        id: creditCard.number,
                        number: creditCard.number,
                        holdername: creditCard.holderName,
                        type: creditCard.type,
                        cvv: creditCard.cvv,
                        expirationMonth: creditCard.expirationMonth,
                        expirationYear: creditCard.expirationYear,
                        balance: 1000,
                        customer: {
                            id: creditCard.account.id,
                            name: creditCard.account.name,
                            address: "UFC",
                            country: "USA",
                        }
                    }
                })
            );
            await sendRequest();


        } catch (error) {

        }
    };
};



export const updateBalanceCreditCard = (data) => {
    //To get card selected

    return async (dispatch) => {

        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards/${data.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');
            }
        };

        try {
            await sendRequest();
            dispatch(
                creditCardActions.setCurrentCreditCard({ currentCreditCard: data })
            );

            dispatch(
                creditCardActions.updateBalance({ id: data.id, balance: data.balance })
            );




        } catch (error) {
        }
    };
};

export const updateBalanceCreditCardDestination = (data) => {
    //To get card selected

    return async (dispatch) => {

        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/cards/${data.id}`,
                {
                    method: 'PUT',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');
            }
        };

        try {
            await sendRequest();
        } catch (error) {
        }
    };
};