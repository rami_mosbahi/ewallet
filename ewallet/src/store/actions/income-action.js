

import env from "../../config"

import moment from 'moment';
import { incomeActions } from '../reducers/income-slice';

export const addIncome = (income) => {
    return async (dispatch) => {
        const idIncome = "INC" + Date.now()

        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/incomes`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: idIncome,
                        destination: income.destination,
                        destinationCard: income.destinationCard,
                        amount: income.amount,
                        type: income.type,
                        from: income.from,
                        date: moment().format()
                    })
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');
            }
        };

        try {

            await sendRequest();


        } catch (error) {

        }
    };
};


export const getAllIncomes = () => {
    return async (dispatch) => {


        const fetchData = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/incomes`
            );

            if (!response.ok) {
                throw new Error('Could not fetch cart data!');

            }

            const data = await response.json();

            return data;
        };

        try {
            const getAllIncomes = await fetchData();
            dispatch(
                incomeActions.setIncomes({
                    incomes: getAllIncomes || [],
                })
            );

        } catch (error) {

        }
    };
};
