
import { updateBalanceCreditCard, updateBalanceCreditCardDestination } from './creditCard-action';
import { expenseActions } from '../reducers/expense-slice';
import { addIncome } from './income-action';

import env from "../../config"
import { useSelector } from 'react-redux'
import moment from 'moment';



const idCustomer = (option) => {
    if (option == 'Khabib')
        return 1
    if (option == 'Conor')
        return 2
    if (option == 'Daniel')
        return 3

}

export const addExpense = (expense, getCurrentCard, getDestinationCard) => {
    return async (dispatch) => {
        const idExpense = "EXP" + Date.now()

        const sendRequest = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/expenses`,
                {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        id: idExpense,
                        destination: idCustomer(expense.destination),
                        destinationCard: getDestinationCard.id,
                        amount: expense.amount,
                        type: expense.type,
                        from: expense.from,
                        date: moment().format()
                    })
                }
            );

            if (!response.ok) {
                throw new Error('Sending cart data failed.');
            }
        };

        try {

            await sendRequest().then(() => {
                dispatch(
                    addIncome({
                        destination: idCustomer(expense.destination),
                        destinationCard: getDestinationCard.id,
                        amount: parseInt(expense.amount),
                        type: expense.type,
                        from: expense.from,
                    })
                )

                dispatch(
                    updateBalanceCreditCard({
                        id: getCurrentCard.id,
                        number: getCurrentCard.number,
                        holdername: getCurrentCard.holdername,
                        type: getCurrentCard.type,
                        cvv: getCurrentCard.cvv,
                        expirationMonth: getCurrentCard.expirationMonth,
                        expirationYear: getCurrentCard.expirationYear,
                        balance: getCurrentCard.balance - expense.amount,
                        customer: {
                            id: getCurrentCard.customer.id,
                            name: getCurrentCard.customer.name,
                            address: getCurrentCard.customer.address,
                            country: getCurrentCard.customer.country,
                            photo: getCurrentCard.customer.photo,
                        }
                    })
                )



                dispatch(
                    updateBalanceCreditCardDestination({
                        id: getDestinationCard.id,
                        number: getDestinationCard.number,
                        holdername: getDestinationCard.holdername,
                        type: getDestinationCard.type,
                        cvv: 777,
                        expirationMonth: getDestinationCard.expirationMonth,
                        expirationYear: getDestinationCard.expirationYear,
                        balance: (parseInt(getDestinationCard.balance) + parseInt(expense.amount)),
                        customer: {
                            id: getDestinationCard.customer.id,
                            name: getDestinationCard.customer.name,
                            address: getDestinationCard.customer.address,
                            country: getDestinationCard.customer.country,
                            photo: getDestinationCard.customer.photo,
                        }
                    })
                )


            });




        } catch (error) {

        }
    };
};


export const getAllExpenses = () => {
    return async (dispatch) => {


        const fetchData = async () => {
            const response = await fetch(
                `${env.BACKEND_URL}/expenses`
            );

            if (!response.ok) {
                throw new Error('Could not fetch cart data!');

            }

            const data = await response.json();

            return data;
        };

        try {
            const getAllExpenses = await fetchData();
            dispatch(
                expenseActions.setExpenses({
                    expenses: getAllExpenses || [],
                })
            );

        } catch (error) {

        }
    };
};