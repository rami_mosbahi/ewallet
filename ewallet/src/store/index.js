import { configureStore } from '@reduxjs/toolkit';

import uiSlice from './reducers/ui-slice';
import creditCardSlice from './reducers/creditCard-slice';
import expenseSlice from './reducers/expense-slice';
import incomeSlice from './reducers/income-slice';


const store = configureStore({
    reducer: {
        ui: uiSlice.reducer,
        creditCard: creditCardSlice.reducer,
        expense: expenseSlice.reducer,
        income: incomeSlice.reducer,
    },
});

export default store;